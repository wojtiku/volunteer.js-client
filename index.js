var request = require('request');
var Q = require('q');
var MAX_RETRIES = 100;

function functionName(fun) {
    var ret = fun.toString();
    ret = ret.substr('function '.length);
    ret = ret.substr(0, ret.indexOf('('));
    return ret;
}

function computeNotFound() {
    var result = Q.defer();
    result.reject("Compute function not found.");
    return result.promise;
}

module.exports = function VolounteerJsClient(host) {
    if (host.length > 0 && host.substr(-1) === "/") {
        host = host.substring(0, host.length - 1)
    }

    var VolounteerJsClient = function map(fn, data, callback) {
        var computationPromise =  createComputation(fn)
            .then(function computationCreated(computationId) {
                return addTasksToComputation(computationId, data);
            })
            .then(function taskAddedToComputation(computationId) {
                return getComputationResults(computationId);
            });
        if (typeof callback === "function") {
            computationPromise.then(function (results) {
                callback(results, data);
            });
        }
        return computationPromise;
    };

    var createComputation = VolounteerJsClient.prototype.createComputation = function createComputation(fn) {
        var deferred = Q.defer();
        if ((typeof fn === "function" && functionName(fn) !== "compute") ||
            (typeof fn === "string" && fn.indexOf("compute") === -1)) {
            return computeNotFound();
        }
        var code = fn.toString();

        request.post({
            url: host + "/rest/computation",
            json: {code: code}
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                var createdId = body;
                deferred.resolve(createdId);
            } else {
                deferred.reject(error);
            }
        });

        return deferred.promise;
    };

    var addTasksToComputation = VolounteerJsClient.prototype.addTasksToComputation = function addTasksToComputation(computationId, data) {
        var deferred = Q.defer();
        if (!Array.isArray(data)) {
            data = [data];
        }

        request.post({
            url: host + "/rest/computation/" + computationId,
            json: data
        }, function (error, response) {
            if (!error && response.statusCode == 200) {
                deferred.resolve(computationId);
            } else {
                deferred.reject(error);
            }
        });

        return deferred.promise;
    };

    var getComputationResults = VolounteerJsClient.prototype.getComputationResults = function getComputationResults(computationId) {
        var deferred = Q.defer();

        var retries = 0;
        var requestResults = function() {
            request.get({
                url: host + "/rest/computation/" + computationId,
                json: true
            }, function (error, response, result) {
                if (!error && response.statusCode == 202) {
                    var retryTimeout = result.pending * 100;
                    retries++;
                    if (retries <= MAX_RETRIES) {
                        setTimeout(requestResults, retryTimeout);
                    } else {
                        deferred.error({
                            code: "fetch-results",
                            computationId: computationId,
                            message: "Results timeout for"
                        });
                    }
                } else if (!error && response.statusCode == 200) {
                    deferred.resolve(result);
                } else if (error && error.code == "ETIMEDOUT") {
                    retries++;
                    if (retries <= MAX_RETRIES) {
                        setTimeout(requestResults, 100);
                    } else {
                        deferred.error({
                            code: "fetch-results",
                            computationId: computationId,
                            message: "Results timeout for"
                        });
                    }
                } else {
                    deferred.reject(error);
                }
            });
        };
        requestResults();
        return deferred.promise;
    };
    return VolounteerJsClient;
};